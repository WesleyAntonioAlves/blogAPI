# Blog API
## API para consumo em blog pessoal

#### ℹ Sobre a Blog API

Aplicação construida para atender a blog construido 100% em HTML, CSS, JavaScript. Aplicação com funcionalidade
básica como por exemplo, criação de novos posts, listagem de post, exibição de post específico, etc.

#### 🔑 Autenticação

É necessá possuir cadastro no sistema para que possa consumir os dados por meio da API,
no momento em que o cadastro é feito é gerada uma chave única para o usuário,
chave essa que é utilizada para autenticar o usuário da API e o domínio a que a chave está ligada,
para garantir que não haverá duplicidade de acesso as informações para manipulação, por meio de terceiros.
caso seja necessário o acesso a mesma conta em outro domínio deve-se gerar uma nova chave e vincula-la ao
novo domínio.

#### 🔐 Segurança

Após a autenticação e retornado um _TOKEN_ [**JWT**](https://jwt.io/introduction) (JSON WEB TOKEN)  que deve ser utilizado para fazer as requizuições,
e de tempos em tempos este _TOKEN_ é atualizado.
