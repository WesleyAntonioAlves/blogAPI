<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;

class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('posts')->insert([
            '_id' => Uuid::uuid4(),
            'title' => 'Demo title',
            'subtitle' => 'Viverra tellus in hac habitasse platea dictumst vestibulum rhoncus est pellentesque elit ullamcorper dignissim cras. Porttitor lacus, luctus accumsan tortor posuere ac ut consequat semper viverra nam libero justo, laoreet!',
            'article' => 'Nibh cras pulvinar mattis nunc, sed! Nec nam aliquam sem et tortor consequat id porta nibh venenatis cras sed felis eget velit aliquet sagittis id consectetur purus ut faucibus pulvinar?
Blandit volutpat maecenas volutpat blandit aliquam etiam. Risus pretium quam vulputate dignissim suspendisse in est ante in nibh mauris, cursus mattis molestie a, iaculis at erat pellentesque adipiscing commodo elit!
Enim blandit volutpat maecenas volutpat blandit aliquam etiam erat velit, scelerisque! Rhoncus, urna neque viverra justo, nec ultrices dui sapien eget mi proin sed libero enim, sed faucibus turpis in?
Tellus cras adipiscing enim eu turpis egestas pretium aenean pharetra, magna ac placerat vestibulum, lectus. Pretium viverra suspendisse potenti nullam ac tortor vitae purus faucibus ornare suspendisse sed nisi lacus?',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        
    }
}
