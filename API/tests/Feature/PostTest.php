<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

use App\Http\Controllers\PostController;
use Faker\Provider\Lorem;

class PostTest extends TestCase
{


    public function testWriteNewPost()
    {
       
        $data = [
            'title' => 'habitasse platea dictumst',
            'subtitle' => 'Viverra tellus in hac habitasse platea dictumst',
            'article' => ' Sapien eget mi proin sed libero enim, sed faucibus turpis in eu mi bibendum neque egestas congue? Id faucibus nisl tincidunt eget nullam non nisi est, sit amet facilisis magna!'
        ];

        $response = $this->json('POST', '/api/post/new', $data);
        $response->assertStatus(200);
    }
    
    public function testReadAllPosts()
    {
        $response = $this->json('GET', '/api/post/all');
        $response->assertStatus(200);
    }

}
