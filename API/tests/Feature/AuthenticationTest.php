<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class AuthenticationTest extends TestCase
{

    public function testAuthenticateUserApi() {
        $credentials = [
            'email' => 'wesley.alves.jhenp@gmail.com',
            'password' => '123123123'
        ];
        
        $response = $this->json('POST', '/api/auth/login', $credentials);
        if($response) {
            $response->assertStatus(401);
        }
    }  

}
