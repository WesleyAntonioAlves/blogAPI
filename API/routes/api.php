<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\AuthController;
use App\Http\Controllers\PostController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });


Route::post('auth/login', [AuthController::class, 'login']);
 
Route::middleware(['jwt'])->group(function () {
    Route::get('auth/me', [AuthController::class, 'me']); 
    Route::get('auth/logout', [AuthController::class, 'logout']); 
    Route::get('auth/refresh', [AuthController::class, 'refresh']); 
    Route::get('/users', [UserController::class, 'index']); 
});

Route::prefix('/post')->group(function() {
    Route::get('/all', [PostController::class, 'index']);
    Route::post('/new', [PostController::class, 'store']);
});

