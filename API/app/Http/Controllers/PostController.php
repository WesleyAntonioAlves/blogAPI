<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
use Ramsey\Uuid\Uuid;

class PostController extends Controller
{
   
    public function index(int $limit = null, string $order = 'asc') {

        if($limit === null)
            $posts = Post::select('*')
                   ->get();

        $posts = Post::select('*' )
               ->orderBy('id', $order)
               ->limit($limit)
               ->get();

        return response()->json($posts);
    }

    
    public function store(Request $request) {

        $request->validate([
            'title' => 'required|string|min:12|max:120',
            'subtitle' => 'required|string|min:20|max:255',
            'article' => 'required|string'
        ]);

        
        $post = Post::create([
            '_id' => Uuid::uuid4(),
            'title' => $request->title,
            'subtitle' => $request->subtitle,
            'article' => $request->article
        ]);

        if($post->save())
            return response()->json($post);
        else
            return response()->json('Error');
        
    }

}
